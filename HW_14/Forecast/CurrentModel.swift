import Foundation
import RealmSwift
import  Realm

class CityWeather: Object,  Decodable {
    @objc dynamic var main: Main?
}

class Main: Object, Decodable {
    @objc dynamic var pressure: Int
    @objc dynamic var humidity: Int
    @objc dynamic var temp: Double
}

