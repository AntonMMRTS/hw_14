import Foundation
import Alamofire
import RealmSwift

protocol ForecastLoaderDelegate2 {
    func loadedWeather(weather:[List1])
}

class ForecastLoader2 {
    
    private let realm = try! Realm()
    
    var delegate: ForecastLoaderDelegate2?
    
    func weather2() {
        var forecastArray: [List1] = []
        AF.request("https://api.openweathermap.org/data/2.5/forecast?q=Moscow&appid=d931fa462f74e60a23984d4b55410584").responseJSON {
            response in
                guard let data = response.data else { return }
            do {
                try! self.realm.write {
                let forecast = try JSONDecoder().decode(Forecast.self, from: data)

                forecastArray = forecast.list

                self.realm.add(forecastArray)
                }
                    self.delegate?.loadedWeather(weather: forecastArray)
            }
        }
    }
}

