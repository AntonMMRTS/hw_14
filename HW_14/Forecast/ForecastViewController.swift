import UIKit
import Alamofire
import RealmSwift

class ForecastViewController: UIViewController {
    
    private let realm = try! Realm()
    private var array = [Main]()
    
    @IBOutlet weak var humLabel: UILabel!
    @IBOutlet weak var presLabel: UILabel!
    @IBOutlet weak var tempLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    var weather: [List1] = []
    var weather1: [List1] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        currentWeather()
        internetDisabled()
       
        let loader = ForecastLoader2()
        loader.delegate = self
        loader.weather2()
    }
    
    func currentWeather() {
        var temper: Double?
        var pressure: Int?
        var humidity: Int?
        AF.request("https://api.openweathermap.org/data/2.5/weather?q=Moscow&appid=d931fa462f74e60a23984d4b55410584").responseJSON {
            response in
                guard let data = response.data else { return }

                do {
                    try self.realm.write {
                    let model = try JSONDecoder().decode(CityWeather.self, from: data)
                    temper = model.main!.temp
                    pressure = model.main!.pressure
                    humidity = model.main!.humidity

                            self.tempLabel.text = "\(Int(temper! - 273))°C"
                            self.humLabel.text = "\(humidity!)%"
                            self.presLabel.text = "\(pressure!) гПа"
                            self.realm.add(model)
                    }
                }
                catch let error {
                    print(error)
                }
            }
    }
    
    func internetDisabled() {
        weather1 = realm.objects(List1.self).map({$0 })
        array = realm.objects(Main.self).map({$0 })
        
        if !weather1.isEmpty {
            for _ in 0..<40 {
                weather.insert(weather1.last!, at: 0)
                weather1.removeLast()
            }
        } else {
            weather = []
        }
        
        if array.last != nil {
            humLabel.text = "\(array.last!.humidity)%"
            tempLabel.text = "\(Int(array.last!.temp - 273))°C"
            presLabel.text = "\(array.last!.pressure) гПа"
        } else {
            humLabel.text = ""
            tempLabel.text = ""
            presLabel.text = ""
        }
    }
}

extension ForecastViewController: ForecastLoaderDelegate2 {
    func loadedWeather(weather:[List1]) {
        self.weather = weather
        tableView.reloadData()
    }
}

extension ForecastViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return weather.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "WeatherCell") as! ForecastTableViewCell
        
        let model = weather[indexPath.row]
        cell.dateLabel.text = "\(model.dt_txt)"
        cell.tempLabel.text = "\(Int(model.main!.temp - 273))°C"
        cell.pressureLabel.text = "\(model.main!.pressure) гПа"
        cell.humidityLabel.text = "\(model.main!.humidity)%"
        return cell
    }
}
