import Foundation
import RealmSwift

class Forecast: Decodable {
    var list: [List1]
}

class List1: Object, Decodable {
    @objc dynamic var main: Main2?
    @objc dynamic var dt_txt: String
}

class Main2: Object, Decodable {
    @objc dynamic var temp: Double
    @objc dynamic var pressure: Int
    @objc dynamic var humidity: Int
}
