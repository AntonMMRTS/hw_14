import UIKit
import CoreData

class Entry2ViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var dateTextField: UITextField!
    
    let datePicker = UIDatePicker()
    
    private let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    public var complitionHandler: (()->Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        dateSetup()
        textField.delegate = self
        textField.becomeFirstResponder()
        datePicker.setDate(Date(), animated: true)
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Save", style: .done, target: self, action: #selector(didTapeSaveButton))
    }
    
    @IBAction func didTapeSaveButton() {
        if let text = textField.text, !text.isEmpty {
            let date = datePicker.date
            let newItem = ToDoListItem2(context: context)
            newItem.date = date
            newItem.item = text
            do {
                try context.save()
            } catch let error  {
                print(error)
            }
            complitionHandler?()
            navigationController?.popToRootViewController(animated: true)
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func dateSetup() {
        dateTextField.inputView = datePicker
        datePicker.preferredDatePickerStyle = .wheels
        datePicker.datePickerMode = .date
        let toolBar = UIToolbar()
        toolBar.sizeToFit()
        let doneButton = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(doneAction))
        let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        toolBar.setItems([flexSpace, doneButton], animated: true)
        dateTextField.inputAccessoryView = toolBar
    }
    
    @objc func doneAction() {
        getDateFromPicker()
        view.endEditing(true)
    }
    
    func getDateFromPicker() {
        let formatter = DateFormatter()
        formatter.dateStyle = .medium
        dateTextField.text = formatter.string(from: datePicker.date)
    }
}
