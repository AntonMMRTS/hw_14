import UIKit
import CoreData

class ToDoCoreDataViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var table: UITableView!
    private let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    private var data = [ToDoListItem2]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        table.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        table.delegate = self
        table.dataSource = self
        getAllItems()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        cell.textLabel?.text = data[indexPath.row].item
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let item = data[indexPath.row]
        
        guard let vc = storyboard?.instantiateViewController(identifier: "view2") as? View2ViewController else {return}
        vc.item = item
        vc.deletionHandler = { [weak self] in
            self?.refresh()
        }
            vc.navigationItem.largeTitleDisplayMode = .never
            vc.title = item.item
            navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func didTapAddButton() {
        guard let vc = storyboard?.instantiateViewController(identifier: "entry2") as? Entry2ViewController else { return }
        vc.complitionHandler = { [weak self] in
            self?.refresh()
        }
        vc.title = "New item"
        vc.navigationItem.largeTitleDisplayMode = .never
        navigationController?.pushViewController(vc, animated: true)
    }
    
    func refresh() {
        do {
            data = try context.fetch(ToDoListItem2.fetchRequest())
        } catch let error  {
            print(error)
        }
        table.reloadData()
    }
    
    func getAllItems() {
        do {
            data = try context.fetch(ToDoListItem2.fetchRequest())
        } catch let error  {
            print(error)
        }
    }


}

