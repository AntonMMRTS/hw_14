//
//  ToDoListItem+CoreDataProperties.swift
//  HW_14
//
//  Created by Антон Усов on 30.03.2021.
//
//

import Foundation
import CoreData


extension ToDoListItem2 {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<ToDoListItem2> {
        return NSFetchRequest<ToDoListItem2>(entityName: "ToDoListItem")
    }

    @NSManaged public var date: Date
    @NSManaged public var item: String?

}

extension ToDoListItem2 : Identifiable {

}
