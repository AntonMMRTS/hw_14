import Foundation

class Persistence {
    static var shared = Persistence()
    
    private let kFirstNameKey = "Persistence.kNameKey"
    private let kLastNameKey = "Persistence.kLastNameKey"
    
    var firstName: String? {
        set {
            UserDefaults.standard.set(newValue, forKey: kFirstNameKey)
        }
        get {
            UserDefaults.standard.string(forKey: kFirstNameKey)
        }
    }
    var lastName: String? {
        set {
            UserDefaults.standard.set(newValue, forKey: kLastNameKey)
        }
        get {
            UserDefaults.standard.string(forKey: kLastNameKey)
        }
    }
}

