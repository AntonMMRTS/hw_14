import UIKit

class UserDefaultsViewController: UIViewController, UITextFieldDelegate {
    
    @IBAction func saveButton(_ sender: Any) {
        let firstName = firstNameTextField.text
        Persistence.shared.firstName = firstName
        let lastName = lastNameTextField.text
        Persistence.shared.lastName = lastName
    }
    
    @IBOutlet weak var lastNameTextField: UITextField!
    @IBOutlet weak var firstNameTextField: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        lastNameTextField.delegate = self
        firstNameTextField.delegate = self
       
        firstNameTextField.text = Persistence.shared.firstName
        lastNameTextField.text = Persistence.shared.lastName
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
